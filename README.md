# Trocar React Dice 

## Install

```bash
npm install trocar-react-dice
```

## Use

```jsx
import React from 'react';
import Dice from 'trocar-react-dice/dice';

export default ()=><>

Dice Roller:

<Dice>

</>
```

## Options

```jsx
<Dice theme="default"/>
<Dice theme="dark"/>
<Dice collapsed={true}/>
<Dice collapsed={false}/>

```

## Advanced use



```jsx
import React,{useState} from 'react';
import DiceRoller from 'trocar-react-dice/dice-roller';
        
        export default ()=>{
         const [result,setResult] = useState(-1); 
         return <>
            <DiceRoller theme={theme} onResult={(dieCount, dieSides,  result)=>{
            setResults([...results,{
            results: result,
            sides: dieSides,
            count: dieCount,
            time: new Date()
            }])

            <h1>You got a {result}</h1>
        }}/></>
        }

```




