import React, {useState} from 'react';
import DiceRoller from './dice-roller';
import DiceResults from './dice-results';

export const Dice = ({theme="default",collapsed=false})=>{
    const [results, setResults]=useState([]);
    return <>
        <DiceRoller theme={theme} onResult={(dieCount, dieSides,  result)=>{
            setResults([...results,{
            results: result,
            sides: dieSides,
            count: dieCount,
            time: new Date()
            }])
        }}/>
        <DiceResults collapsed={collapsed} theme={theme} results={results}/>
    </>}
export default Dice;

