import React, {useState} from 'react'; 
import Dice from 'trocar-dice-js';
import './dice.css';
export const DiceRoller=({onResult, theme="default"})=>{
    const [dieCount, setDieCount]=useState(1);
    const [dieSides, setDieSides]=useState(6);
return <div className={"trocar-dice-"+theme+"-theme"}>
<input 
    className="trocar-dice-dieCount"
    type="number" 
    min="1" 
    max="100" 
    value={dieCount} 
    onChange={(x)=>setDieCount(x.target.value)} ></input>D
<select 
      className="trocar-dice-dieSides"
      value={dieSides}
      onChange={e => setDieSides(e.currentTarget.value)}>
  <option value="2">2</option>
  <option value="4">4</option>
  <option value="6">6</option>
  <option value="8">8</option>
  <option value="10">10</option>
  <option value="12">12</option>
  <option value="20">20</option>
  <option value="100">100</option>
</select>
<button 
    className="trocar-dice-roll-button"
    onClick={()=>onResult(parseInt(dieCount),parseInt(dieSides),Dice.dice(parseInt(dieCount),parseInt(dieSides)))}>Roll</button>
</div>
}
export default DiceRoller;