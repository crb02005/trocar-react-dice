import React,{useRef, useEffect } from 'react'; 
import './dice.css';
export const DiceResult = ({results,sides,count,time,focused,theme="default"})=>{
const ref = useRef(null);

const scrollToBottom = () => {
    ref.current.scrollIntoView({ behavior: "smooth" })
  }

  useEffect(scrollToBottom, [focused]);

return <div ref={ref} className={"trocar-dice-"+theme+"-theme trocar-dice-result "+(focused?"trocar-dice-result-focused":"")}>
At <span title={time.toString()}>{time.toLocaleTimeString()}</span> you rolled a {sides} sided die {count} time{count>1?"s":""}. You got {results.reduce((a,b)=>a+b,0)} ({results.join()})</div>

}
export default DiceResult;
