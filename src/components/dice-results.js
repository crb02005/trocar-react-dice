import React from 'react';
import DiceResult from './dice-result';
import './dice.css';

export const DiceResults = ({results, collapsed, theme="default"})=>{
    return <div className={"trocar-dice-"+theme+"-theme trocar-dice-results"}>
      {collapsed? 
        results.length > 0? <DiceResult {...results[results.length-1]} focused={true} theme={theme}></DiceResult>
        :""
        :results.map((result,index)=><DiceResult key={index} {...result} focused={index===results.length-1} theme={theme}></DiceResult>
      )}
    </div>}
export default DiceResults;

