import Dice from './components/dice';
import React, { useState } from 'react';

function App() {
  const [collapsed,setCollapsed]=useState(false);
  const [theme,setTheme]=useState(false);
  
  return (
    <div className="App">
      Example Options:
        <br/>
        Collapsed: <input type="checkbox" value={collapsed} onChange={(x)=>setCollapsed(x.target.checked)} />
        <br/>
        Theme: <select value={theme} onChange={(x)=>setTheme(x.target.value)}>
            <option value="default">Default</option>
            <option value="dark">Dark</option>
        </select><br/>
      Example:<br/>
      <Dice theme={theme} collapsed={collapsed}/>

    </div>
    
  );
}

export default App;
